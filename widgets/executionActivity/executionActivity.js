widget = {
    //runs when we receive data from the job
    onData: function(el, data) {

        //The parameters our job passed through are in the data object
        //el is our widget element, so our actions should all be relative to that
        if (data.title) {
            $('h2', el).text(data.title);
        }
        var executionHtml = "<table><tr><th>Summary</th><th>Status</th></tr>";

        data.data.executions.forEach(function(execution, index){
            executionHtml += "<tr><td>"+execution.issueSummary+"</td><td title=''"+execution.status.description+"' style='color:"+execution.status.color+"'>"+ execution.status.name+" <td></tr>"
        })
        executionHtml += "</table>"
        $('.content', el).html(executionHtml);
        $('#widgets-container').css("border:1px solid; border-color:#B0B0B0")
    }
};