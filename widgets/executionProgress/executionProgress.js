widget = {
    //runs when we receive data from the job
    onData: function(el, data) {

        //The parameters our job passed through are in the data object
        //el is our widget element, so our actions should all be relative to that
        if (data.title) {
            $('h2', el).text(data.title);
        }

        var executionHtml = "<table><tr><th>Status</th><th>Count</th></tr>";
        data.series.forEach(function(statusTotals, index){
            executionHtml += "<tr><td>"+statusTotals.label+"</td><td title=''"+statusTotals.label+"' style='color:"+statusTotals.color+"'>"+ statusTotals.count+" <td></tr>"
        })
        executionHtml += "</table>"
        $('.content', el).html(executionHtml);
    }
};