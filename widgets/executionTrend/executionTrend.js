widget = {
    //runs when we receive data from the job
    onData: function(el, data) {
        //The parameters our job passed through are in the data object
        //el is our widget element, so our actions should all be relative to that
        if (data.title) {
            $('h2', el).text(data.title);
        }
        var graph;
        if ($(".chart", el).hasClass('rickshaw_graph')){
            $(".chart", el).empty();
        }

        var palette = new Rickshaw.Color.Palette();
        for (var i = 0; i < data.series.length; i++) {
            if(!data.series[i].color)
                data.series[i].color = palette.color();
        }

        var graph = new Rickshaw.Graph({
            element: $(".chart", el)[0],
            renderer: data.chartType || 'line',
            height: $(el).closest('li').height() - 50,
            stroke: true,
            series: data.series
        });
        
        if (data.xAxis){
            var xAxis = new Rickshaw.Graph.Axis.Time({
                graph: graph
            });
            xAxis.render();
        }

        if (data.yAxis){
            var yAxis = new Rickshaw.Graph.Axis.Y({
                graph: graph
            });
            yAxis.render();
        }

        var hoverDetail = new Rickshaw.Graph.HoverDetail( {
            graph: graph,
            xFormatter: function(x) {return x + "seconds" },
            yFormatter: function(y) {return y}

        } );
        $("#legend").empty();
        var legend = new Rickshaw.Graph.Legend( {
            graph: graph,
            element: document.getElementById('legend')

        } );

        graph.render();
    }
};