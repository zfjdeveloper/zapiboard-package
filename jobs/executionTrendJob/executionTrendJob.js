/**
 * Job: executionTrendJob
 *
 * Expected configuration:
 *
 * { }
 */
var querystring = require('querystring'),
    cache = require('memory-cache');

module.exports = function(config, dependencies, job_callback) {
    var username = config.username || config.globalAuth.zapi.username
    var password = config.password || config.globalAuth.zapi.password
    var jiraserver = config.jira_server || config.globalAuth.zapi.jira_server
    var path = config.path || config.globalAuth.zapi.path
    var timeout = config.timeout || config.globalAuth.zapi.timeout || 150000
    if (!username || !password){
        return job_callback('non credentials found in execution Trends job. Please check config.globalAuth');
    }

    if (!jiraserver || !path){
        return job_callback('missing JIRA Server OR path parameters in Execution Trend Job, please check config.globalAuth');
    }

    var text = "Execution Trend added !";

    //projectId, daysPrevious, groupFld, periodName.
    var params = {
        projectId       : config.projectId || config.globalAuth.options.projectId || 10000,
        daysPrevious    : config.daysPrevious || 30,
        groupFld        : "timePeriod",
        periodName      : config.periodName || "daily"
    };

    var options = {
        timeout: timeout,
        url: jiraserver + path + "execution/count?"  + querystring.stringify(params),
        headers: {
            "authorization": "Basic " + new Buffer(username + ":" + password).toString("base64")
        }
    };


    var cache_expiration = 60 * 1000; //ms
    var cache_key = 'zapi-executionTrend:config-' + JSON.stringify(config); // unique cache object per job config
    if (cache.get(cache_key)){
        return job_callback (null, cache.get(cache_key));
    }

    dependencies.request(options, function(error, response, dataJSON) {
        if (error || !response || (response.statusCode != 200)) {
            var err_msg = (error || (response ? ("bad statusCode: " +
                response.statusCode) : "bad response")) + " from " + options.url;
            job_callback(err_msg);
        }
        else {
            var responseData;

            try {
                responseData = JSON.parse(dataJSON);
            }
            catch (err){
                var msg = 'error parsing JSON response from server';
                return job_callback(msg);
            }

            /** TODO - To fetch it via ZAPI */
            responseData.statusSeries ? statusSeries = responseData.statusSeries : statusSeries = {"1":{color:"#75B000", name:"PASS"},
                                                                                                    "2":{color:"#CC3300",name:"FAIL"},"3":{color:"#F2B000", name:"WIP"},
                                                                                                    "4":{color:"#6693B0", name:"BLOCKED"},"5":{color:"#0033ff", name:"CANCEL"},
                                                                                                    "6":{color:"#ff99ff", name:"ONHOLD"},"7":{color:"#996666", name:"OBSOLETE"},
                                                                                                    "total":{color:"#F0F0F0", name:"TOTAL"},"executed":{color:"#00FF00", name:"EXECUTED"},
                                                                                                    "-1":{color:"#A0A0A0", name:"UNEXECUTED"}
                                                                                                   };
            responseData.data ? chartData = responseData.data : chartData = [];

            var execStatusSeries = [];
            var seriesTemp = {};	//Object to hold series data temporarily. Once its populated, we will flush this data into execStatusSeries
            var xTicks = [];

            /*Iterate over all the column data, one at a time*/
            var seriesCumulative = {};
            for (var timeInMillis in responseData.data){
                /*Now, lets dynamically create the chart series */
                if(responseData.data[timeInMillis]["total"] == 0)
                    continue;
                for (var statusId in responseData.data[timeInMillis]){
                    var series = seriesTemp[statusId];
                    seriesCumulative[statusId] = seriesCumulative[statusId] || 0;
                    if(!series){
                        series = [];
                        seriesTemp[statusId] = series;
                    }
                    var statusCnt = responseData.data[timeInMillis][statusId];
                    seriesCumulative[statusId] = seriesCumulative[statusId] + statusCnt;
                    series.push({x:parseInt(timeInMillis/1000), y:(config.cumulative?seriesCumulative[statusId]: statusCnt)});

                }
                //xTicks.push([parseInt(i), AJS.escapeHtml(responseData.data[i].name)]);
            }

            for(var ser in seriesTemp){
                //seriesTemp[ser].push([args.schedules.data.length + 0.5, 0]);
                if(statusSeries[ser]){
                    execStatusSeries.push({name:statusSeries[ser].name, data:seriesTemp[ser], color:statusSeries[ser].color});
                }
            }


            var data = {
                title: config.widgetTitle,
                series: execStatusSeries,
                chartType: config.chartType || 'line',
                xAxis: config.xAxis,
                yAxis: config.yAxis,
                groupFld: params.groupFld
            };
            cache.put(cache_key, data, cache_expiration); //add to cache
            job_callback(null, data);
        }
    });
};
