/**
 * Job: executionActivityJob
 *
 * Expected configuration:
 * 
 * { }
 */
var querystring = require('querystring'),
    cache = require('memory-cache');

module.exports = function(config, dependencies, job_callback) {
    var username = config.username || config.globalAuth.zapi.username
    var password = config.password || config.globalAuth.zapi.password
    var jiraserver = config.jira_server || config.globalAuth.zapi.jira_server
    var path = config.path || config.globalAuth.zapi.path
    var timeout = config.timeout || config.globalAuth.zapi.timeout || 150000

    if (!username || !password){
        return job_callback('non credentials found in execution Trends job. Please check config.globalAuth');
    }

    if (!jiraserver || !path){
        return job_callback('missing JIRA Server OR path parameters in Execution Trend Job, please check config.globalAuth');
    }

    var text = "Execution Activity added !";

    var params = {
        zqlQuery    : "project= "+ config.project +" AND executionStatus != 'UNEXECUTED' ORDER BY ExecutionDate DESC",
        offset      : 0,
        maxRecords  : 0,
        expand      : "executionStatus"
    };

    var options = {
        timeout: timeout,
        url: jiraserver + path + "zql/executeSearch/?"  + querystring.stringify(params),
        headers: {
            "authorization": "Basic " + new Buffer(username + ":" + password).toString("base64")
        }
    };


    var cache_expiration = 60 * 1000; //ms
    var cache_key = 'zapi-executionActivity:config-' + JSON.stringify(config); // unique cache object per job config
    if (cache.get(cache_key)){
        return job_callback (null, cache.get(cache_key));
    }

    dependencies.request(options, function(error, response, dataJSON) {
        if (error || !response || (response.statusCode != 200)) {
            var err_msg = (error || (response ? ("bad statusCode: " +
                response.statusCode) : "bad response")) + " from " + options.url;
            job_callback(err_msg);
        }
        else {
            var responseData;

            try {
                responseData = JSON.parse(dataJSON);
            }
            catch (err){
                var msg = 'error parsing JSON response from server';
                return job_callback(msg);
            }

            var data = {
                title: config.widgetTitle,
                data: responseData
            };
            cache.put(cache_key, data, cache_expiration); //add to cache
            job_callback(null, data);
        }
    });
};
