/**
 * Job: executionTrendJob
 *
 * Expected configuration:
 *
 * { }
 */
var querystring = require('querystring'),
    cache = require('memory-cache');

module.exports = function(config, dependencies, job_callback) {
    var username = config.username || config.globalAuth.zapi.username
    var password = config.password || config.globalAuth.zapi.password
    var jiraserver = config.jira_server || config.globalAuth.zapi.jira_server
    var path = config.path || config.globalAuth.zapi.path
    var timeout = config.timeout || config.globalAuth.zapi.timeout || 150000
    if (!username || !password){
        return job_callback('non credentials found in execution Trends job. Please check config.globalAuth');
    }

    if (!jiraserver || !path){
        return job_callback('missing JIRA Server OR path parameters in Execution Trend Job, please check config.globalAuth');
    }

    var text = "Execution Trend added !";

    //projectId, daysPrevious, groupFld, periodName.
    var params = {
        projectId       : config.projectId || config.globalAuth.options.projectId || 10000,
        versionId       : config.versionId || config.globalAuth.options.versionId || -1,
        groupFld        : "cycle"
    };

    var options = {
        timeout: timeout,
        url: jiraserver + path + "execution/count?"  + querystring.stringify(params),
        headers: {
            "authorization": "Basic " + new Buffer(username + ":" + password).toString("base64")
        }
    };


    var cache_expiration = 60 * 1000; //ms
    var cache_key = 'zapi-executionProgress:config-' + JSON.stringify(config); // unique cache object per job config
    if (cache.get(cache_key)){
        return job_callback (null, cache.get(cache_key));
    }

    dependencies.request(options, function(error, response, dataJSON) {
        if (error || !response || (response.statusCode != 200)) {
            var err_msg = (error || (response ? ("bad statusCode: " +
                response.statusCode) : "bad response")) + " from " + options.url;
            job_callback(err_msg);
        }
        else {
            var responseData;

            try {
                responseData = JSON.parse(dataJSON);
            }
            catch (err){
                var msg = 'error parsing JSON response from server';
                return job_callback(msg);
            }

            var statusSeries = responseData.statusSeries || {1:"75B000",2:"CC3300",3:"F2B000",4:"6693B0",5:"0033ff","total":"FF0000", "executed":"00FF00", "-1":"A0A0A0"};
            var chartData = responseData.data || [];

            var execStatusSeries = [];
            var seriesTemp = {};	//Object to hold series data temporarily. Once its populated, we will flush this data into execStatusSeries

            /*Iterate over all the column data, one at a time*/
            for (var i in responseData.data){
                /*Now, lets dynamically create the chart series */
                for (var statusId in responseData.data[i].cnt){
                    var series = seriesTemp[statusId] || 0;
                    series += responseData.data[i].cnt[statusId];
                    seriesTemp[statusId] = series;
                }
            }

            for(var ser in seriesTemp){
                //seriesTemp[ser].push([args.schedules.data.length + 0.5, 0]);
                var statusId = parseInt(ser)
                var unExecutedCount = seriesTemp["-1"];
                if(statusSeries[statusId]){
                    if(statusId == -1)
                        continue;
                    execStatusSeries.push({label:statusSeries[statusId].name, count:seriesTemp[ser], color:statusSeries[statusId].color});
                }
            }
            /*Total*/
            execStatusSeries.unshift({label:"UNEXECUTED", count:unExecutedCount, color:"#808080"});
            /*Executed*/
            execStatusSeries.unshift({label:"EXECUTED", count:(seriesTemp["total"] - unExecutedCount) , color:"#E0E0E0"});


            var data = {
                title: config.widgetTitle,
                series: execStatusSeries
            };
            cache.put(cache_key, data, cache_expiration); //add to cache
            job_callback(null, data);
        }
    });
};
