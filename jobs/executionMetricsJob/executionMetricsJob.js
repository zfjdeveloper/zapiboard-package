var querystring = require('querystring'),
    cache = require('memory-cache');

module.exports = function(config, dependencies, job_callback) {

    var username = config.username || config.globalAuth.zapi.username
    var password = config.password || config.globalAuth.zapi.password
    var jiraserver = config.jira_server || config.globalAuth.zapi.jira_server
    var path = config.path || config.globalAuth.zapi.path
    var timeout = config.timeout || config.globalAuth.zapi.timeout || 150000

    if (!username || !password) {
        return job_callback('non credentials found in execution Trends job. Please check config.globalAuth');
    }
    if (!jiraserver || !path) {
        return job_callback('missing JIRA Server OR path parameters in Execution Trend Job, please check config.globalAuth');
    }

    var statusSeries = {};
    var chartData = [];
    var logger = dependencies.logger;
    var restUrl = "execution/count?"

    var params = {
        groupFld: "cycle",
        projectId: config.projectId || config.globalAuth.options.projectId || 10000,
        versionId: config.versionId || config.globalAuth.options.versionId || 10000
    };

    var options = {
        timeout: timeout,
        url: jiraserver + path + restUrl + querystring.stringify(params),
        headers: {
            "authorization": "Basic " + new Buffer(username + ":" + password).toString("base64")
        }
    };


    var cache_expiration = 60 * 1000; //ms
    var cache_key = 'zapi-testexecgraph:config-' + JSON.stringify(config); // unique cache object per job config
    if (cache.get(cache_key)) {
        return job_callback(null, cache.get(cache_key));
    }

    dependencies.request(options, function (error, response, dataJSON) {
        if (error || !response || (response.statusCode != 200)) {
            var err_msg = (error || (response ? ("bad statusCode: " +
                response.statusCode) : "bad response")) + " from " + options.url;
            job_callback(err_msg);
        }
        else {
            var responseData;

            try {
                responseData = JSON.parse(dataJSON);
            }
            catch (err) {
                var msg = 'error parsing JSON response from server';
                return job_callback(msg);
            }

            responseData.statusSeries ? statusSeries = responseData.statusSeries : statusSeries = {};
            responseData.data ? chartData = responseData.data : chartData = [];

            function pupulateChartData(indx) {
                var dataCol = [];
                for (var i = 0; i < chartData.length; i++) {
                    var obj = chartData[i];
                    dataCol.push({ x: i, y: obj.cnt[indx]});
                }
                ;
                return dataCol;
            }

            var series = [];
            for (var i = 0; i < config.numberSeries; i++) {
                var indx = i + 1;
                if (indx == 5)
                    indx = -1;
                series.push({data: pupulateChartData(indx), color: statusSeries[indx].color});
            }
            ;


            var data = {
                title: config.widgetTitle,
                series: series,
                chartType: config.chartType || 'bar',
                xAxis: config.xAxis,
                yAxis: config.yAxis,
                groupFld: params.groupFld
            };
            cache.put(cache_key, data, cache_expiration); //add to cache
            job_callback(null, data);
        }
    });
};
